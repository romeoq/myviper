// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

enum MyViperError: String, Error {
    case intIsNull = "Полученная цифра равна нулю"
}
