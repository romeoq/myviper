// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class AnotherAssembly: AnotherAssemblyType {
    
    func configure(with view: AnotherViewType & ViewType) {
        let presenter = AnotherPresenter(view: view)
        let interactor = AnotherInteractor(presenter: presenter)
        let router = AnotherRouter(view: view)
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
