// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class AnotherInteractor {
    
    private weak var presenter: AnotherPresenterType!
    
    required init(presenter: AnotherPresenterType) {
        self.presenter = presenter
    }
    
}

//MARK: InteractorType
extension AnotherInteractor: AnotherInteractorType {
    
}
