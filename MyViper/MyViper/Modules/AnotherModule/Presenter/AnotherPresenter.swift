// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class AnotherPresenter {
    
    private weak var view: AnotherViewType!
    var interactor: AnotherInteractorType!
    var router: AnotherRouterType!
    
    required init(view: AnotherViewType) {
        self.view = view
    }
    
}

//MARK: PresenterType
extension AnotherPresenter: AnotherPresenterType {
    func configureView() {
        
    }
}
