// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol AnotherPresenterType: class {
    var router: AnotherRouterType! { get set }
    
    func configureView()
}
