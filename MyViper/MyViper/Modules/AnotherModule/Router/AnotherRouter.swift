// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class AnotherRouter {
    
    private weak var view: ViewType!
    
    init(view: ViewType) {
        self.view = view
    }
    
}

extension AnotherRouter: AnotherRouterType {
    
}
