// Created by Roman Voinitchi on 8/12/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

final class AnotherViewController: UIViewController {
    
    var presenter: AnotherPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        let assembly: AnotherAssemblyType = AnotherAssembly()
        assembly.configure(with: self)
        presenter.configureView()
    }

}

extension AnotherViewController: AnotherViewType {
   
}

extension AnotherViewController: ViewType {
    var viewController: UIViewController {
        return self
    }
}
