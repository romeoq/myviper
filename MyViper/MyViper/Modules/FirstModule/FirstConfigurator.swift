// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol FirstConfiguratorType: class {
    func configure(with viewController: FirstView)
}

//MARK: Отвечает за конфигурацию модуля (assembly)
class FirstConfigurator: FirstConfiguratorType {
    
    func configure(with viewController: FirstView) {
        let presenter = FirstPresenter(view: viewController)
        let interactor = FirstInteractor(presenter: presenter)
        let router = FirstRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
