// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol FirstInteractorType: class {
    
}

//MARK: Отвечает за связь с сервером
class FirstInteractor: FirstInteractorType {
    
    weak var presenter: FirstPresenterType!
    //Инициализация сервисов
    
    required init(presenter: FirstPresenterType) {
        self.presenter = presenter
    }
    
}
