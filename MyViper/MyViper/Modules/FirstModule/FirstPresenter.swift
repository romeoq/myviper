// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol FirstPresenterType: class {
    var router: FirstRouterType! { get set }
    func configureView()
    func secondBtnTaped()
}

//MARK: Отвечает за представление данных в вью
class FirstPresenter: FirstPresenterType {
    
    weak var view: FirstViewType!
    var interactor: FirstInteractorType!
    var router: FirstRouterType!
    
    required init(view: FirstViewType) {
        self.view = view
    }
    
    func configureView() {
        //TODO конфигурация вьюшки, если нужно
    }
    
    func secondBtnTaped() {
        router.openSecondViewController()
    }
    
    
}
