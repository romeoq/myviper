// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol FirstRouterType: class {
    func openSecondViewController()
}

//MARK: Отвечает за переходы между экранами
class FirstRouter: FirstRouterType {
    
    weak var viewController: FirstView!
    
    init(viewController: FirstView) {
        self.viewController = viewController
    }
    
    func openSecondViewController() {
        viewController.performSegue(withIdentifier: "onSecondSegue", sender: nil)
    }
    
}
