// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

protocol FirstViewType: class {
    
}

class FirstView: UIViewController, FirstViewType {
    
    var presenter: FirstPresenterType!
    private let configurator: FirstConfiguratorType = FirstConfigurator()

    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    @IBAction func onSecondBtnTap(_ sender: Any) {
        presenter.secondBtnTaped()
    }
    
}
