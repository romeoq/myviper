// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class HelperConfigurator: HelperConfiguratorType {
    
    func configure(with view: HelperViewType & ViewType, textDelegate: HelperTextDelegate) {
        let presenter = HelperPresenter(view: view, textDelegate: textDelegate)
        let interactor = HelperInteractor(presenter: presenter)
        let router = HelperRouter(view: view)
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
