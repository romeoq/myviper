// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol HelperConfiguratorType {
    func configure(with view: HelperViewType & ViewType, textDelegate: HelperTextDelegate)
}
