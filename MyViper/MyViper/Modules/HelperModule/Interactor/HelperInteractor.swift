// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class HelperInteractor {
    
    private weak var presenter: HelperPresenterType!
    
    required init(presenter: HelperPresenterType) {
        self.presenter = presenter
    }
    
}

//MARK: InteractorType
extension HelperInteractor: HelperInteractorType {
    
}
