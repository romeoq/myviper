// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class HelperPresenter {
    
    private weak var view: HelperViewType!
    var interactor: HelperInteractorType!
    var router: HelperRouterType!
    
    private unowned var textDelegate: HelperTextDelegate!
    
    required init(view: HelperViewType, textDelegate: HelperTextDelegate) {
        self.view = view
        self.textDelegate = textDelegate
    }
    
}

//MARK: PresenterType
extension HelperPresenter: HelperPresenterType {
    func configureView() {
        view.setLabelText(labelText: textDelegate.textToShow)
    }
}
