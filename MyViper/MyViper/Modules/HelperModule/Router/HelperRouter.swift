// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

final class HelperRouter {
    
    private weak var view: ViewType!
    
    init(view: ViewType) {
        self.view = view
    }
    
}

//MARK: RouterType
extension HelperRouter: HelperRouterType {
    func closeModule() {
        self.view.viewController.dismiss(animated: true, completion: nil)
    }
}


