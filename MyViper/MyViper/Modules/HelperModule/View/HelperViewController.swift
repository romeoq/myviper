// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

final class HelperViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    var presenter: HelperPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.configureView()
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        self.presenter.router.closeModule()
    }
    
}

extension HelperViewController: HelperViewType {
    func setLabelText(labelText: String?) {
        textLabel.text = labelText
    }
}

extension HelperViewController: ViewType {
    var viewController: UIViewController {
        return self
    }
}
