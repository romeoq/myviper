// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol HelperViewType: class {
    var presenter: HelperPresenterType! { get set }
    
    func setLabelText(labelText: String?)
}
