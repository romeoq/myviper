// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

//MARK: ConfiguratorProtocol
protocol SecondConfiguratorType {
    func configure(with view: SecondViewType & ViewType)
}

//MARK: ConfiguratorClass (Initializer, Assembly)
class SecondConfigurator: SecondConfiguratorType {
    
    func configure(with view: SecondViewType & ViewType) {
        let presenter = SecondPresenter(view: view)
        let interactor = SecondInteractor(presenter: presenter)
        let router = SecondRouter(view: view)
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
