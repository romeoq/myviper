// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

//MARK: InteractorProtocol
protocol SecondInteractorType {
    func getNotNullIntFromServer(completion: @escaping (Result<Int8, MyViperError>) -> ())
}

//MARK: InteractorClass
class SecondInteractor {
    
    //MARK: presenter reference
    private weak var presenter: SecondPresenterType!
    
    //MARK: Services
    
    
    required init(presenter: SecondPresenterType) {
        self.presenter = presenter
    }
    
}

//MARK: InteractorType
extension SecondInteractor: SecondInteractorType {
    func getNotNullIntFromServer(completion: @escaping (Result<Int8, MyViperError>) -> ()) {
        //Использовать инициализированные сервисы
        let newInt = Int8.random(in: -3 ... 3)
        if newInt == 0 {
            completion(.failure(.intIsNull))
        } else {
            completion(.success(newInt))
        }
    }
}
