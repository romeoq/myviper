// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

//MARK: PresenterProtocol
protocol SecondPresenterType: class {
    var router: SecondRouterType! { get set }
    
    func configureView()
    func getNewInt()
    func openHelperView()
}

//MARK: PresenterClass
class SecondPresenter {
    
    private weak var view: SecondViewType!
    var interactor: SecondInteractorType!
    var router: SecondRouterType!
    
    private var currentValue: Int8 = 0 {
        didSet {
            self.view.setLabelValue(newValue: currentValue)
        }
    }
    
    required init(view: SecondViewType) {
        self.view = view
    }

}

//MARK: PresenterType
extension SecondPresenter: SecondPresenterType {
    
    func configureView() {
        view.setLabelValue(newValue: currentValue)
    }
    
    func getNewInt() {
        interactor.getNotNullIntFromServer { result in
            switch result {
            case .success(let newInt):
                self.currentValue = newInt
            case .failure(let myViperError):
                self.view.processError(error: myViperError)
            }
        }
    }
    
    func openHelperView() {
        router.openHelperView(with: self)
    }
    
}

extension SecondPresenter: HelperTextDelegate {
    var textToShow: String? {
        return String(currentValue)
    }
}
