// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

//MARK: RouterProtocol
protocol SecondRouterType {
    func openAnotherStoryboard()
    func openHelperView(with textDelegate: HelperTextDelegate)
}

//MARK: RouterClass
class SecondRouter {
    
    private weak var view: ViewType!
    
    init(view: ViewType) {
        self.view = view
    }
    
}

//MARK: RouterType
extension SecondRouter: SecondRouterType {
    func openAnotherStoryboard() {
        let newStoryboard = UIStoryboard(name: "Another", bundle: nil)
        let navController = newStoryboard.instantiateViewController(withIdentifier: "AnotherNavController")
        navController.modalPresentationStyle = .fullScreen
        self.view.viewController.present(navController, animated: true, completion: {
            print("New Nav controller called")
        })
    }
    
    func openHelperView(with textDelegate: HelperTextDelegate) {
        
        guard let helperViewController = UIStoryboard(name: "Helpers", bundle: nil).instantiateViewController(identifier: "HelperViewController") as? HelperViewController else {
            return
        }
        
        let helperConfigurator: HelperConfiguratorType = HelperConfigurator()
        helperConfigurator.configure(with: helperViewController, textDelegate: textDelegate)

        view.viewController.present(helperViewController, animated: true, completion: nil)
    }
}
