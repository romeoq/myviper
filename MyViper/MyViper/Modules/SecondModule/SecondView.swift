// Created by Roman Voinitchi on 8/10/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

//MARK: ModuleProtocol
protocol SecondViewType: class {
    var presenter: SecondPresenterType! { get set }
    
    func setLabelValue(newValue: Int8)
    func processError(error: MyViperError)
}

//MARK: ModuleClass
class SecondView: UIViewController {
    
    //MARK: Outlets
    @IBOutlet private weak var numberLabel: UILabel!
    
    //MARK: Presenter
    var presenter: SecondPresenterType!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let configurator: SecondConfiguratorType = SecondConfigurator()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    //MARK: IBActions
    @IBAction func onAnotherBtnTap(_ sender: Any) {
        presenter.router.openAnotherStoryboard()
    }
    
    @IBAction func onHelperTap(_ sender: Any) {
        presenter.openHelperView()
    }
    
    @IBAction func onChangeValueTap(_ sender: Any) {
        presenter.getNewInt()
    }
    
}

//MARK: ModuleType
extension SecondView: SecondViewType {
    func setLabelValue(newValue: Int8) {
        numberLabel.text = String(newValue)
    }
    
    func processError(error: MyViperError) {
        let alert = UIAlertController(title: "Ошибка", message: error.rawValue, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

//MARK: Common ViewType
extension SecondView: ViewType {
    var viewController: UIViewController {
        return self
    }
}
