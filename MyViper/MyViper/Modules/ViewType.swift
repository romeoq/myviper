// Created by Roman Voinitchi on 8/11/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

protocol ViewType: class {
    var viewController: UIViewController { get }
}
